//changes database and changes generators
//combining changes to a longer sequence (or even song parts, maybe this as a seperate 'module')
//variation generator (repeating, or random every time)
//chord rythmn placer (different algoritmns, euclidean, standard templates,...)
//	|
//	V
//bass module		chord module		melody module
//placement of		voicingvariations	Different scales on chords
//notes on rythmn				counterpoint with bass
//counterpoint with melody
//looping in chord without movement
//(play closest note)

//to read:
//https://www.ijcai.org/Proceedings/15/Papers/346.pdf

inlets = 4;
outlets = 5;

var note = 0;
var progression = 0;
var sequencelength =16;

var chordsequence;

//if (jsarguments.length>1)
//	myval = jsarguments[1];

function msg_int(v)
{
	//post("received note " + v + "\n");
	
	if (inlet==0) {
		note = v;
		bang();
	}
	if (inlet==1) {
		progression = v;
		note = 0;//remove later!
	}
	if (inlet==3) {
		sequencelength = v;
		note = 0;//remove later!
	}

}

function anything(){
	var themessage = arrayfromargs(messagename, arguments);
	if (inlet==2) {
		var chord = themessage[0];
		var toplay = chords.show(chord);
		//post("\n"+toplay);
		outlet(4,toplay);
		//chord = s[0];
		//bang();
	}
}

function bang()
{
	//post("\nnote"+note+" "+Progressions.show(progression)[note]+" "+Progressions.VARIATIONS.length);
	chordtimemap();
	
	
	outlet(0,chords.show(chordsequence[note]));
	outlet(1,Progressions.show(progression)[note]);
	outlet(2,Progressions.show(progression));
	
	outlet(3,chordsequence);
	note++;
	note = note%chordsequence.length;
	
}

function chordtimemap(){
	chordsequence = euclideanRhythm(Progressions.VARIATIONS[progression].changes.length,sequencelength);
	var ch =0;	
	for(var i = 0; i<chordsequence.length; i++){
		if(chordsequence[i]){
			chordsequence[i]=Progressions.show(progression)[ch];
			ch++;
		}
	}
}

function euclideanRhythm(onNotes, totalNotes) {
	//source: https://github.com/dbkaplun/euclidean-rhythm/blob/master/euclidean-rhythm.js
  var groups = [];
  for (var i = 0; i < totalNotes; i++) groups.push([Number(i < onNotes)]);

  var l;
  while (l = groups.length - 1) {
    var start = 0, first = groups[0];
    while (start < l && compareArrays(first, groups[start])) start++;
    if (start === l) break;

    var end = l, last = groups[l];
    while (end > 0 && compareArrays(last, groups[end])) end--;
    if (end === 0) break;

    var count = Math.min(start, l - end);
    groups = groups
      .slice(0, count)
      .map(function (group, i) { return group.concat(groups[l - i]); })
      .concat(groups.slice(count, -count));
  }
  return [].concat.apply([], groups);
};

function compareArrays (a, b) {
  // TODO: optimize
  return JSON.stringify(a) === JSON.stringify(b);
};

var Progressions = {
	VARIATIONS : [
		//from https://www.hooktheory.com/theorytab/common-chord-progressions
	    {
		name : 'The Most Popular Progression',
		changes : ['I','V','vi','IV']	
	    },		
	    {
		name : 'Pachelbel\'s Progression',
		changes : ['I','V','vi','III','IV']	
	    },
		{
		name : 'Effective In All Genres',
		changes : ['vi','V','IV','V']	
	    },
		{
		name : 'Those Magic Changes',
		changes : ['I','vi','IV','V']	
	    },
		{
		name : 'Gaining Popularity',
		changes : ['I','IV','vi','V']	
	    },		
	    {
		name : 'Timeless',
		changes : ['I','V','IV','V']	
	    },
		{
		name : 'The Cadential 64',
		changes : ['IV','ii','I64','V']	
	    },
		{
		name : 'Stepwise Bass Down',
		changes : ['I','V6','vi','V']	
	    },
		{
		name : 'Stepwise Bass Up',
		changes : ['I','ii7','I6','IV']	
	    },		
	    {
		name : 'The Newcomer',
		changes : ['I','iii64','vi','IV']	
	    },
		{
		name : 'I6 As A Precadence',
		changes : ['IV','I6','V']	
	    },
		{
		name : 'Simple Yet Powerful',
		changes : ['IV','I6','ii']	
	    },
		{
		name : 'The Chord That Pleased The Lord',
		changes : ['IV','V6/vi','vi']	
	    },		
	    {
		name : 'Expanding With V/Vi',
		changes : ['I','V/vi','vi']	
	    },
		{
		name : 'Vi42 As A Passing Chord',
		changes : ['vi','vi42','IV']	
	    },
		{
		name : 'Secondary Dominant',
		changes : ['I','V7/IV','IV']	
	    },	
		{
		name : 'Applied Viio',
		changes : ['V','viio/vi','vi']	
	    },		
	    {
		name : 'Cadencing In Style',
		changes : ['IV','iv','I']	
	    },
		{
		name : 'A Mixolydian Cadence',
		changes : ['I','bVII','IV']	
	    },
		{
		name : 'Those Magic Changes',
		changes : ['I','vi','IV','V']	
	    },
	 	{
		name : 'Using The bVI To Set Up A V',
		changes : ['I','bVI','V']	
	    },		
	    {
		name : 'Cadencing Via ♭VII',
		changes : ['I','V','IV','bVII']	
	    },
	],
	
	
	show : function (i) {
		
		//post("\ni: "+i);
		//post("\nProgression name: "+ this.VARIATIONS[i].name);
		return this.VARIATIONS[i].changes;//modulo to make sure it always gives an answer
	},
	
	amount : function(){
		return this.VARIATIONS.length;
	}
}

var chords = {
	//inspired by: https://github.com/naiquevin/GuitarJs/blob/master/src/guitar.js


		
	//notes are as midi offset from the center
	TYPES:[
	//via https://en.wikipedia.org/wiki/Chord_names_and_symbols_(popular_music)
		{
			name: 'major',
			refScale : ['major'],
			notes : [0,4,7]
		},{
			name: 'minor',
			refScale : ['minor'],
			notes : [0,3,7]
		},{
			name : 'augmented',
			refScale : ['major'],
			notes : [0,4,8]		
	    }, {
			name : 'diminished',
			refScale : ['major'],
			notes : [0,3,6]			
	    },{
			name : 'sus2',
			refScale : ['major'],
			notes : [0,2,7]		
	    },{
			name : 'sus4',
			refScale : ['major'],
			notes : [0,5,7]			
	    },

		{
			name : '7',
			refScale : ['major'],
			notes : [0,4,7,10]			
	    },{
			name : 'major7',
			refScale : ['major'],
			notes : [0,4,7,11]			
	    },{
			name : 'minor7',
			refScale : ['major'],
			notes : [0,3,7,10]			
	    },{
			name : 'dim7',
			refScale : ['major'],
			notes : [0,3,6,9]			
	    },{
			name : 'halfdim7',
			refScale : ['major'],
			notes : [0,3,6,10]			
	    },{
			name : 'aug7',
			refScale : ['major'],
			notes : [0,4,8,10]			
	    }


	],
	

	show : function (input) {
		var debug =1;		
		var offset = 0;
		var inversion = 0;
		var chordname;
		
		var bassnote = -1;
		
		output = input;
		
		if(input==0)input=lastinput;
		else lastinput=input;
		
		if(debug)post("\ninput = "+ input);
	//figuring out the chord:
	
	
	//check if lowered or highered ( b or #) and trip
		//output = i.indexOf('b');
		if (input.indexOf('b')==0){
			offset--;
			input =  input.substr(1);
			//post("\nflattened chord "+ input);
		} else if (input.indexOf('#')==0){
			offset++;
			input =  input.substr(1);
			//post("\nsharpened chord "+ input);
		}
		if(debug)post("\nflat or sharp = "+ offset);
		
	//check chord type (upper or lowercase) major minor
		if (input.charAt(0)==input.charAt(0).toUpperCase()) chordname = 'major';
		else chordname = 'minor';
		if(debug)post("\nchord = "+chordname) ;
		
	//check what is the chord bass (eg, I or ii or..)
		if(input.indexOf('VII')==0){
			offset+=11;
			input =  input.substr(3);//trim it yo
		} else if(input.indexOf('vii')==0){
			offset+=10;
			input =  input.substr(3);//trim it yo
		} else if(input.indexOf('VI')==0){
			offset+=9;
			input =  input.substr(2);//trim it yo
		} else if(input.indexOf('vi')==0){
			offset+=8;
			input =  input.substr(2);//trim it yo
		} else if(input.indexOf('v')==0||input.indexOf('V')==0){
			offset+=7;
			input =  input.substr(1);//trim it yo
		} else if(input.indexOf('iv')==0||input.indexOf('IV')==0){
			offset+=5;
			input =  input.substr(2);//trim it yo
		} else if(input.indexOf('III')==0){
			offset+=4;
			input =  input.substr(3);//trim it yo
		} else if(input.indexOf('iii')==0){
			offset+=3;
			input =  input.substr(3);//trim it yo
		} else if(input.indexOf('ii')==0||input.indexOf('II')==0){
			offset+=2;
			input =  input.substr(2);//trim it yo
		} else if(input.indexOf('i')==0||input.indexOf('I')==0){
			offset+=0;
			input =  input.substr(1);//trim it yo
		} else if(debug)post("\nsome weird ass wrong input yo ");


		if(debug)post("\noffset = "+ offset);
		
//check if adition (7, 9, o, aug, ...)
		if(input.indexOf('sus2')==0){
			chordname = 'sus2';
			input =  input.substr(4);
		} else if(input.indexOf('sus4')==0){
			chordname = 'sus4';
			input =  input.substr(4);
		} else if(input.indexOf('7')==0){
			chordname = chordname.concat('7');
			input =  input.substr(1);
		}  else  if(input.indexOf('o7')==0){//or dim7?
			chordname = 'dim7';
			input =  input.substr(2);
		} else  if(input.indexOf('+')==0){//or aug?
			chordname = 'aug';
			input =  input.substr(1);
		} else  if(input.indexOf('halfdim')==0){
			chordname = 'halfdim';
			input =  input.substr(8);
		} else  if(input.indexOf('o')==0){//or dim
			chordname = 'dim';
			input =  input.substr(1);
		} else  if(input.indexOf('add6')==0){
			chordname = chordname.concat('6');
			input =  input.substr(4);
		} 
			
		if(debug)post("\nchord = "+ chordname);
//check for inversion and adjust order

  		if(input.indexOf('64')==0){
			inversion = 2;
			input =  input.substr(2);
		}else  if(input.indexOf('65')==0){
			inversion = 1;
			input =  input.substr(2);
		}else  if(input.indexOf('43')==0){
			inversion = 2;
			input =  input.substr(2);
		}else  if(input.indexOf('42')==0){
			inversion = 3;
			input =  input.substr(2);
		}else  if(input.indexOf('2')==0){
			inversion = 3;
			input =  input.substr(1);
		}else if(input.indexOf('6')==0){
			inversion = 1;
			input =  input.substr(1);
		} 

		if(debug)post("\ninversion = "+ inversion);
		
//check for slash chord and adjust bass

		if(input.indexOf('/')==0){
			input =  input.substr(1);	
			if(input.indexOf('VII')==0)bassnote=11;
			else if(input.indexOf('vii')==0)bassnote=10;
			else if(input.indexOf('VI')==0)bassnote=9;
			else if(input.indexOf('vi')==0)bassnote=8;
			else if(input.indexOf('v')==0||input.indexOf('V')==0)bassnote=7;
			else if(input.indexOf('iv')==0||input.indexOf('IV')==0)bassnote=5;
			else if(input.indexOf('III')==0)bassnote=4;
			else if(input.indexOf('iii')==0)bassnote=3;
			else if(input.indexOf('ii')==0||input.indexOf('II')==0)bassnote=2;
			else if(input.indexOf('i')==0||input.indexOf('I')==0)bassnote=0;
		}
		if(debug)post("\nbassnote = "+ bassnote);
			
//get the actual chord notes
		//var chordnotes = new var;
	    for(var i in this.TYPES){
	 		if(chordname == this.TYPES[i].name){
	 	    	chordnotes = this.TYPES[i].notes.slice();//slice is to make a copy instead of a reference
	 		}			
	    }	
		for(var i in chordnotes){//offset for chord root
			chordnotes[i]+=offset;
		}
		
		//inversions
		//if(inversion)chordnotes = chordnotes.concat(Number(chordnotes.splice(0,inversion))+12).slice();
		for (;inversion>0;inversion--)chordnotes = chordnotes.concat(Number(chordnotes.splice(0,1))+12).slice();
		
		
		//slash
		if (bassnote>0){
			if (bassnote >chordnotes[0])bassnote-=12;
			chordnotes.unshift(bassnote);
		}
	
		if(debug)post("\nchord = "+ chordnotes);

		
	
		return chordnotes;//this.TYPES[0].notes;
	},
}

var scale = {
}
