
inlets = 1;
outlets = 4;

var note = 0;

//if (jsarguments.length>1)
//	myval = jsarguments[1];

function msg_int(v)
{
	//post("received note " + v + "\n");
	note = v;
	bang();
}

function bang()
{
	outlet(0,note);
	outlet(1,note+5);
	outlet(2,note+9);
	outlet(3,euclideanRhythm(6, 8));
}

function euclideanRhythm(onNotes, totalNotes) {
	//source: https://github.com/dbkaplun/euclidean-rhythm/blob/master/euclidean-rhythm.js
  var groups = [];
  for (var i = 0; i < totalNotes; i++) groups.push([Number(i < onNotes)]);

  var l;
  while (l = groups.length - 1) {
    var start = 0, first = groups[0];
    while (start < l && compareArrays(first, groups[start])) start++;
    if (start === l) break;

    var end = l, last = groups[l];
    while (end > 0 && compareArrays(last, groups[end])) end--;
    if (end === 0) break;

    var count = Math.min(start, l - end);
    groups = groups
      .slice(0, count)
      .map(function (group, i) { return group.concat(groups[l - i]); })
      .concat(groups.slice(count, -count));
  }
  return [].concat.apply([], groups);
};

function compareArrays (a, b) {
  // TODO: optimize
  return JSON.stringify(a) === JSON.stringify(b);
};