inlets = 7;
outlets = 8;

var bars=0;
var beats=0;
var resolution=0;
var units=0;
var timesigtop =4;
var timesigbottom =4;

var tnew = [7];
var told=[7];

function msg_int(v)
{
	//post();
	
	if (inlet==0) {//bars
		bars=v-1;
	}
	if (inlet==1) {//beats
		beats=v-1;
	}
	if (inlet==4) {//tempo, unused
	
	}
	if (inlet==6) {//transport state, unused
	
	}

}

function msg_float(f)
{
	if (inlet==2) {//units
		//units=f;
		progress(f);
	}
		if (inlet==3) {//resolution
		resolution=f;
	}
}

function anything(){
	if (inlet==5) {//time signature
		var timesig = arrayfromargs(messagename, arguments);
		timesigtop = timesig[0];
		timesigbottom = timesig[1];
	}
}

function bang(){//empty
}

function progress(units){

	tnew[0] = units/resolution;//1
	tnew[1] = (beats)/timesigtop;//2
	tnew[2] = (bars/timesigbottom)%(1) ;//4
	tnew[3] = (Math.floor(bars/timesigbottom)/timesigbottom)%1;//8
	tnew[4] = (Math.floor((bars/timesigbottom)/timesigbottom)/timesigbottom)%1;//16
	tnew[5] = (Math.floor(((bars/timesigbottom)/timesigbottom)/timesigbottom)/timesigbottom)%1;//32
	tnew[6] = (Math.floor((((bars/timesigbottom)/timesigbottom)/timesigbottom)/timesigbottom)/timesigbottom)%1;//64
	tnew[7] = (Math.floor(((((bars/timesigbottom)/timesigbottom)/timesigbottom)/timesigbottom)/timesigbottom)/timesigbottom)%1;//128
	
	
	for(var i=0;i<tnew.length;i++){
		//post('\n'+i+'\t'+tnew[i]+'\t'+told[i]);
		if (tnew[i]!=told[i]){
			outlet(i,tnew[i]);
			//post('\nTEST');
			told[i]=tnew[i];
		}
		
	}
	//outlet(0,tnew[0]);
}